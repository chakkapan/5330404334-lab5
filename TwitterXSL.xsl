<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Twitter</title>
                
            </head>
            
            <h2> Twitter @
                    <a>
                        <xsl:attribute name="href">
                        <xsl:value-of select="/rss/channel/link"/>
                        </xsl:attribute>
                        <xsl:value-of select="/rss/channel/link"/>
                    </a>
                </h2>
            <body>
                <TABLE Border="1">
                    <TR>
                        <TD> Title </TD>
                        <TD> Publication Date </TD>
                    </TR>                
                    <xsl:for-each select="/rss/channel/item">
                        <TR>
                            <TD>
                                <a>
                                    <xsl:attribute name="href">
                                    <xsl:value-of select="link"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </TD>
                            <TD>
                                <xsl:value-of select="/rss/channel/item/pubDate"/>
                            </TD>  
                        </TR>
                    </xsl:for-each>
                </TABLE>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
