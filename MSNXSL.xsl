<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>MSN</title>
            </head>
                            
            <body>
                <p style ="color:red"> [ Conversation started on 
                    <xsl:value-of select="/Log/Message/@Date "/> 
                    <xsl:text>  </xsl:text>
                    <xsl:value-of select="/Log/Message/@Time"/>
                    ]
                </p>
                
                <xsl:for-each select="/Log/Message">
                    <p>[<xsl:value-of select="@Date "/> 
                        <xsl:text>  </xsl:text>
                        <xsl:value-of select="@Time"/> ]
                        
                        <xsl:if test="From/User/@FriendlyName = 'Manee'">
                            <span Style="color:#24913C">
                                <xsl:value-of select="From/User/@FriendlyName"/>
                                : <xsl:value-of select="."/>   
                            </span>                            
                        </xsl:if>
                                   
                         
                        <xsl:if test="From/User/@FriendlyName = 'Mana'">
                            <span Style="color:#FFAA00">
                                <xsl:value-of select="From/User/@FriendlyName"/>
                                : <xsl:value-of select="."/>
                            </span>                         
                        </xsl:if>                                
                    </p>
                </xsl:for-each>               
                    
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
